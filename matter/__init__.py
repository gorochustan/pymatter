__pragma__('noanno')

__pragma__('js', '{}', __include__('matter/__javascript__/matter.min.js'))
#import matter.__javascript__.matter as _api

def _ctor(obj):
    '''helper function to create a constructor for a js object'''
    def construct(*args):
        return __new__(obj(*args))
    return construct
    
_api = Matter  #Matter variable is defined by the js included above
__pragma__('js', '{}', 'delete window.Matter;')  #remove the global instance of Matter

#expose the functions and class constructors
Engine = _api.Engine
Render = _api.Render
Runner = _api.Runner
Bodies = _api.Bodies
Composite = _api.Composite
