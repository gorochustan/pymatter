__pragma__ ('skip')
document = window = Math = Date = 0 # Prevent complaints by optional static checker
__pragma__ ('noskip')

from matter import Engine, Render, Runner, Bodies, Composite

#create an an engine
engine = Engine.create()

#create a renderer
render = Render.create({'element' : document.body,
                        'engine' : engine})

#create two boxes and a ground
boxA = Bodies.rectangle(400, 200, 80, 80)
boxB = Bodies.rectangle(450, 50, 80, 80)
ground = Bodies.rectangle(400, 610, 810, 60, {'isStatic' : True})

#add all of the bodies to the world
Composite.add(engine.world, [boxA, boxB, ground])

#run the renderer
Render.run(render)

#create runner
runner = Runner.create()

#run the engine
Runner.run(runner, engine)
